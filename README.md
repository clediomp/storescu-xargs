## STORESCU-XARGS.SH - Import a Directory with huge amount of DICOM files, Tested on CentOS 7 with DCM4CHEE-2.18.3

##### RUN THIS UTILITY WITH NOHUP (Nohup keeps the linux process running, even if you disconect from ssh): 

`nohup sh storescu-xargs.sh &`

##### TO SEE NOHUP OUTPUT LOG:

`tail -f nohup.log` 

##### TO SEE SCRIPT PROCESS:

`ps -aux | grep storescu-xargs.sh`

##### TO SEE NOHUP JOBS:

`jobs -l`

##### IF YOU NEED TO UNZIP AN DIRECTORY WITH MANY ZIP FILES INSIDE, USE:

`unzip "*.zip"`

#### REFERENCE
https://groups.google.com/forum/#!searchin/dcm4che/sending$20large$20amount%7Csort:date/dcm4che/7d33v273qK8/BrwhDMAQAgAJ
