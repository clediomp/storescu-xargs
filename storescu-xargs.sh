#!/bin/sh
##SEND MULTIPLE DIR WITH DICOM FILES TO DCM4CHEE WITH STORESCU AND XARGS
#REFERENCE
##https://groups.google.com/forum/#!searchin/dcm4che/sending$20large$20amount%7Csort:date/dcm4che/7d33v273qK8/BrwhDMAQAgAJ

#SHELL COLORS
BLUE="\033[44;1;37m"
BLACK="\033[0m"

#VARs
IMPORT_DIR="/storage/dicom"
STORESCU="/opt/dcm4che-5.20.0/bin/storescu"
TWIDDLE_PATH="/var/lib/dcm4chee/bin/twiddle.sh"
ADMIN_PASS="admin"

#DICOM SERVER
DCM_AETITLE="DCM4CHEE"
DCM_HOST="localhost"
DCM_PORT="11112"

#XARGS 
PROCESSES="2"
ITEMS="5000"
DATE=$(date '+%d/%m/%Y %H:%M:%S');

#SET StudyDateInFilePath true (DCM4CHEE WILL USE DATE OF STUDY AS DIRECTORY STUCTURE IN STORAGE DIR)
sh $TWIDDLE_PATH -s $DCM_HOST -u admin -p $ADMIN_PASS set "dcm4chee.archive:service=StoreScp" StudyDateInFilePath true
	
echo -e "$BLUE Import started at: $DATE $BLACK \n"
echo -e "$BLUE Import: $IMPORT_DIR $BLACK \n"

#SENDING
find $IMPORT_DIR -type f -print0 | xargs -0 -P $PROCESSES -n $ITEMS $STORESCU -c $DCM_AETITLE@$DCM_HOST:$DCM_PORT

DATE=$(date '+%d/%m/%Y %H:%M:%S');
echo -e "$BLUE Import finished at: $DATE $BLACK \n"